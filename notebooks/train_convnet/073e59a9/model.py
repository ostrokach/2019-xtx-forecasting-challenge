import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset


class XTXDataset(Dataset):
    max_values = None

    def __init__(self, data, num_previous=1, mode="train") -> None:
        super().__init__()
        assert num_previous > 0
        values = data[:, :-1]
        targets = data[:, -1:]

        values = np.flip(values, axis=0)
        targets = np.flip(targets, axis=0)

        self.values = values.reshape(values.shape[0], 4, 15).transpose([1, 2, 0]).copy()
        self.targets = targets.copy()
        self.num_previous = num_previous
        self.mode = mode

    def __len__(self):
        if self.mode == "train":
            return self.targets.shape[0] // self.num_previous - 2
        else:
            return self.targets.shape[0] - self.num_previous + 1

    def __getitem__(self, index):
        if self.mode == "train":
            offset = np.random.randint(0, self.num_previous)
            step = self.num_previous
        else:
            offset = 0
            step = 1

        start = offset + index * step
        stop = start + self.num_previous

        values = self.values[:, :, start:stop]
        #         values = self.values[start:stop, :]
        targets = self.targets[start:stop, :]

        return values, targets


class XTXModel(nn.Module):
    def __init__(
        self,
        dropout=0.5,
        num_previous=1,
        use_previous=False,
        num_hidden=10,
        num_hidden_1=4,
        num_hidden_2=4,
        num_hidden_3=4,
    ):
        super().__init__()
        self.num_previous = num_previous
        self.use_previous = use_previous
        self.num_hidden = num_hidden
        self.num_hidden_3 = 10
        self.num_hidden_4 = 10

        self.layer1 = nn.Sequential(
            #
            nn.Conv1d(60, 32, kernel_size=1, stride=1, padding=0),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Conv1d(32, num_hidden, kernel_size=1, stride=1, padding=0),
        )
        self.norm1 = nn.LayerNorm((num_hidden, num_previous))
        self.dropout1 = nn.Dropout(dropout)

        eye3 = torch.eye(num_hidden)[None, None, :, :, None]
        self.register_buffer("eye3", eye3)
        step_size = self.num_hidden + self.num_hidden_3
        self.layer3 = nn.Sequential(
            #
            nn.Conv2d(
                1, 128, kernel_size=(1, step_size), stride=(1, step_size), padding=0
            ),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Conv2d(128, num_hidden, kernel_size=1, stride=1, padding=0),
            nn.ReLU(),
        )
        self.linear3 = nn.Sequential(
            nn.ReLU(), nn.Conv2d(num_hidden, num_hidden, kernel_size=1, stride=1)
        )
        self.dropout3 = nn.Dropout(dropout)
        self.norm3 = nn.LayerNorm((1, num_hidden, num_previous // self.num_hidden_3))

        eye4 = torch.eye(num_hidden)[None, None, :, :, None]
        self.register_buffer("eye4", eye4)
        step_size = self.num_hidden + self.num_hidden_4
        self.layer4 = nn.Sequential(
            #
            nn.Conv2d(
                1, 128, kernel_size=(1, step_size), stride=(1, step_size), padding=0
            ),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Conv2d(128, num_hidden, kernel_size=1, stride=1, padding=0),
            nn.ReLU(),
        )
        self.linear4 = nn.Sequential(
            nn.ReLU(), nn.Conv2d(num_hidden, num_hidden, kernel_size=1, stride=1)
        )
        self.dropout4 = nn.Dropout(dropout)
        self.norm4 = nn.LayerNorm(
            (1, num_hidden, num_previous // self.num_hidden_3 // self.num_hidden_4)
        )

        self.layer_out = nn.Sequential(
            #
            nn.Conv1d(num_hidden, 64, kernel_size=1, stride=1, padding=0),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Conv1d(64, 1, kernel_size=2, stride=1, padding=0),
        )

        self.reset_parameters()

    def reset_parameters(self):
        pass

    #         nn.init.xavier_uniform_(self.w2, gain=nn.init.calculate_gain("relu"))
    #         nn.init.xavier_uniform_(self.w3, gain=nn.init.calculate_gain("relu"))
    #         nn.init.xavier_uniform_(self.w4, gain=nn.init.calculate_gain("relu"))

    def log_weights(self, writer, epoch):
        pass

    #         writer.add_histogram("Layer2/w", self.w2, epoch)

    def forward(self, x):
        assert x.size(-1) >= self.num_previous

        x = x.reshape(x.size(0), -1, x.size(3))
        x = self.layer1(x)
        x = self.norm1(x)
        x = x.unsqueeze(1)

        x_in = x.view(x.size(0), x.size(1), x.size(2), -1, self.num_hidden_3).transpose(
            -1, -2
        )
        x_in = torch.cat([x_in, self.eye3.expand_as(x_in)], dim=3)
        x_in = x_in.transpose(-2, -1).reshape(x.size(0), x.size(1), x.size(2), -1)

        for i in range(2):
            start = i * self.num_hidden_3
            stop = (i + 1) * self.num_hidden_3
            offset = i * self.num_hidden
            assert (
                (
                    x[:, :, :, start:stop]
                    == x_in[:, :, :, start + offset : stop + offset]
                )
                .all()
                .item()
            )

        x_out = self.layer3(x_in)
        x_out = torch.max(x_out, dim=2, keepdim=True)[0]
        x_out = self.linear3(x_out)
        x_out = x_out.transpose(1, 2)
        x = x[:, :, :, :: self.num_hidden_3] + self.dropout3(x_out)
        x = self.norm3(x)

        x_in = x.view(x.size(0), x.size(1), x.size(2), -1, self.num_hidden_4).transpose(
            -1, -2
        )
        x_in = torch.cat([x_in, self.eye4.expand_as(x_in)], dim=3)
        x_in = x_in.transpose(-2, -1).reshape(x.size(0), x.size(1), x.size(2), -1)

        for i in range(2):
            start = i * self.num_hidden_4
            stop = (i + 1) * self.num_hidden_4
            offset = i * self.num_hidden
            assert (
                (
                    x[:, :, :, start:stop]
                    == x_in[:, :, :, start + offset : stop + offset]
                )
                .all()
                .item()
            )

        x_out = self.layer4(x_in)
        x_out = torch.max(x_out, dim=2, keepdim=True)[0]
        x_out = self.linear3(x_out)
        x_out = x_out.transpose(1, 2)
        x = x[:, :, :, :: self.num_hidden_4] + self.dropout4(x_out)
        x = self.norm4(x)

        x = self.layer_out(x.squeeze(1))
        return x[:, :, 0]


def _get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])
