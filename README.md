# XTX Forecasting Challenge

## Neural network submission

### Did not work

- Changing activation functions (e.g. `LeakyReLU`, `ELU`).
- Using convolutions over both time and feature channels.
- Increasing the number of output channels in the two-layer time convolution.
- Removing `elementwise_affine` transformation from `LayerNorm` non-linearities.
- Increasing the number of hidden channels to 20.
- Last layer 128 without kernel conv.
- Removing `ReLU` after history convs (did not really make much difference).
- Last layer MLP with 64 hidden units and no time conv.

### Undecided

- Using all-by-all convolution on initial features.
 
